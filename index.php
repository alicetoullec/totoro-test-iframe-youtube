<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>

    <?php
    switch($_GET["page"]){
      case "homepage.php";
      echo "Homepage";
      break;
      default;
      echo "ERROR : PAGE NOT FOUND";
      break;
    }?>

  </title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
</head>
<body>

  <header>

    <!-- LOGO FLOTTANT -->

    <div class="logo">
      <div class="circlelogo">
        <a href="http://localhost/risuto/index.php?page=homepage.php"><img src="gallery/noiraude.png" alt=""></a>
      </div>
    </div>

    <!-- MENU -->

    <nav>
      <ul>
        <?php //Je veux que le lien vers la page actuelle soit souligné ?>
        <li><a href="http://localhost/risuto/index.php?page=homepage.php">Homepage</a></li>
        <li><a href="http://localhost/risuto/index.php?page=secondpage.php">Secondpage</a></li>
        <li><a href="#">Three</a></li>
        <li><a href="#">Four</a></li>
        <li><a href="#">Five</a></li>
      </ul>
    </nav>
  </header>

  <!-- CONTENU DE LA PAGE -->

  <?php
  $listPages = array('homepage.php', 'secondpage.php');

  if (isset ($_GET ['page']) && in_array($_GET ['page'], $listPages)) {
    include $_GET['page'];
  }
  else {
    echo "<p class=\"errormessage\">ERROR : PAGE NOT FOUND</p>
    <img class=\"errormessage\" src=\"http://gifsandco.free.fr/gifs/gifs_mangas/totoro/totoro-01.gif\">";
  }
  ?>

</body>
</html>
